# apolo-helm-charts

Repositori amb els helm charts desenvolupats per Apolo Analytics

## Deploy

El deployment es fa automàticament amb un Job CI/CD, només s'ha de crear un tag que segueixi aquest format: `{nom_chart}-v{versio}`, per exemple: `consumer-v0.0.1`.

## Utilitzar aquest repositori

Per utilitzar aquest repositori amb helm només has de fer

```bash
$ helm repo add apolo https://gitlab.com/api/v4/projects/ArnolSwet%2Fapolo-helm-charts/packages/helm/stable
```
```bash
$ helm repo update
```